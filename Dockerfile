FROM debian:stable-slim

MAINTAINER "nixxlab@gmail.com"

WORKDIR /

ADD config.yml /etc/chproxy/config.yml
ADD https://github.com/Vertamedia/chproxy/releases/download/v1.13.2/chproxy-linux-amd64-v1.13.2.tar.gz chproxy.tar.gz

RUN tar -zxf chproxy.tar.gz -C /usr/bin && rm -rf chproxy.tar.gz

ENTRYPOINT chproxy -config /etc/chproxy/config.yml