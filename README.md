# CHProxy

Docker image based on `debian:stable-slim`.

## Usage

```
docker run -d \
  --name=chproxy \
  -v config.yml:/etc/chproxy/config.yml \
  nixxlab/chproxy:latest
```
